package com.atlassian.bitbucket.unapprove;

import com.atlassian.bitbucket.event.repository.RepositoryDeletedEvent;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.unapprove.event.AutoUnapproveSettingsChangedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.apache.commons.lang3.ObjectUtils;

public class AutoUnapproveSettings {

    public static final String PLUGIN_KEY = "com.atlassian.stash.plugin.stash-auto-unapprove-plugin";

    private final EventPublisher eventPublisher;
    private final PluginSettings pluginSettings;

    public AutoUnapproveSettings(EventPublisher eventPublisher, PluginSettingsFactory factory) {
        this.eventPublisher = eventPublisher;
        pluginSettings = factory.createSettingsForKey(PLUGIN_KEY);
    }

    public void disableFor(Repository repository) {
        Object previousValue = pluginSettings.remove(createKey(repository));
        if (previousValue != null) {
            eventPublisher.publish(new AutoUnapproveSettingsChangedEvent(this, repository, false));
        }
    }

    public void enableFor(Repository repository) {
        Object previousValue = pluginSettings.put(createKey(repository), "1");
        if (ObjectUtils.notEqual(previousValue, "1")) {
            eventPublisher.publish(new AutoUnapproveSettingsChangedEvent(this, repository, true));
        }
    }

    public boolean isEnabled(Repository repository) {
        return pluginSettings.get(createKey(repository)) != null;
    }

    @EventListener
    public void onRepositoryDeleted(RepositoryDeletedEvent event) {
        pluginSettings.remove(createKey(event.getRepository()));
    }

    private String createKey(Repository repository) {
        return "repo." + repository.getId() + ".auto.unapprove";
    }
}
