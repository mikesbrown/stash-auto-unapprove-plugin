package com.atlassian.bitbucket.unapprove.event;

import com.atlassian.bitbucket.audit.Channels;
import com.atlassian.bitbucket.audit.Priority;
import com.atlassian.bitbucket.event.annotation.Audited;
import com.atlassian.bitbucket.event.repository.RepositoryEvent;
import com.atlassian.bitbucket.repository.Repository;

import javax.annotation.Nonnull;

@Audited(converter = AutoUnapproveSettingsChangedEventConverter.class, channels = {Channels.REPOSITORY_UI},
        priority = Priority.HIGH)
public class AutoUnapproveSettingsChangedEvent extends RepositoryEvent {

    private final boolean enabled;

    public AutoUnapproveSettingsChangedEvent(@Nonnull Object source, Repository repository, boolean enabled) {
        super(source, repository);
        this.enabled = enabled;
    }

    public boolean getEnabled() {
        return enabled;
    }
}
