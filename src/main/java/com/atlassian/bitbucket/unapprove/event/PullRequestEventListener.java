package com.atlassian.bitbucket.unapprove.event;

import com.atlassian.bitbucket.event.pull.PullRequestRescopedEvent;
import com.atlassian.bitbucket.event.pull.PullRequestUpdatedEvent;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestParticipant;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.unapprove.AutoUnapproveSettings;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.event.api.EventListener;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.Iterables;

public class PullRequestEventListener {

    private final AutoUnapproveSettings autoUnapproveSettings;
    private final PullRequestService pullRequestService;
    private final SecurityService securityService;
    private final TransactionTemplate txTemplate;

    public PullRequestEventListener(AutoUnapproveSettings autoUnapproveSettings, PullRequestService pullRequestService,
                                    SecurityService securityService, TransactionTemplate txTemplate) {
        this.autoUnapproveSettings = autoUnapproveSettings;
        this.pullRequestService = pullRequestService;
        this.securityService = securityService;
        this.txTemplate = txTemplate;
    }

    @EventListener
    public void onPullRequestRescoped(PullRequestRescopedEvent event) {
        if (autoUnapproveSettings.isEnabled(event.getPullRequest().getToRef().getRepository())
                && sourceCommitChanged(event)) {
            unapprovePullRequest(event.getPullRequest());
        }
    }

    @EventListener
    public void onPullRequestUpdated(PullRequestUpdatedEvent event) {
        // Only withdraw approval when the target branch of the pull request changes
        if (autoUnapproveSettings.isEnabled(event.getPullRequest().getToRef().getRepository())
                && (event.getPreviousToBranch() != null)) {
            unapprovePullRequest(event.getPullRequest());
        }
    }

    private void unapprovePullRequest(PullRequest pullRequest) {
        txTemplate.execute(() -> {
            for (PullRequestParticipant participant : Iterables.concat(pullRequest.getReviewers(),
                    pullRequest.getParticipants())) {
                // the withdrawApproval method withdraws approval for the currently authenticated user,
                // so use SecurityService to impersonate the reviewer we are withdrawing approval for
                securityService.impersonating(participant.getUser(), "Unapproving pull-request on behalf of user")
                        .call(() -> pullRequestService.withdrawApproval(
                                pullRequest.getToRef().getRepository().getId(),
                                pullRequest.getId()
                        ));
            }
            return null;
        });
    }

    private boolean sourceCommitChanged(PullRequestRescopedEvent event) {
        return !event.getPreviousFromHash().equals(event.getPullRequest().getFromRef().getLatestCommit());
    }
}
