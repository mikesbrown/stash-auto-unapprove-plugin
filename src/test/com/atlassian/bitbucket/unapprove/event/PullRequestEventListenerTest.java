package com.atlassian.bitbucket.unapprove.event;

import com.atlassian.bitbucket.event.pull.PullRequestRescopedEvent;
import com.atlassian.bitbucket.event.pull.PullRequestUpdatedEvent;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestParticipant;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.repository.Ref;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.unapprove.AutoUnapproveSettings;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.DummySecurityService;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PullRequestEventListenerTest {

    @Mock
    private AutoUnapproveSettings autoUnapproveSettings;
    @Mock
    private PullRequest pullRequest;
    @Mock
    private PullRequestRescopedEvent pullRequestRescopedEvent;
    @Mock
    private PullRequestService pullRequestService;
    @Mock
    private PullRequestUpdatedEvent pullRequestUpdatedEvent;
    @Mock
    private Repository repository;
    private SecurityService securityService = new DummySecurityService();
    @Mock
    private TransactionTemplate txTemplate;

    private PullRequestEventListener underTest;

    @Before
    public void setup() throws Exception {
        // Inject manually to use the DummySecurityService
        underTest = new PullRequestEventListener(autoUnapproveSettings, pullRequestService,
                securityService, txTemplate);

        when(autoUnapproveSettings.isEnabled(any(Repository.class))).thenReturn(true);

        when(pullRequestService.withdrawApproval(anyInt(), anyInt())).thenReturn(mock(PullRequestParticipant.class));
        when(txTemplate.execute(any())).then(invocationOnMock -> {
            TransactionCallback callback = TransactionCallback.class.cast(invocationOnMock.getArguments()[0]);
            return callback.doInTransaction();
        });

        when(pullRequest.getToRef()).thenReturn(mock(PullRequestRef.class));
        when(pullRequest.getFromRef()).thenReturn(mock(PullRequestRef.class));
        when(pullRequest.getToRef().getRepository()).thenReturn(repository);

        PullRequestParticipant pullRequestParticipant = mockParticipant();
        when(pullRequest.getReviewers()).thenReturn(ImmutableSet.of(pullRequestParticipant));

        when(pullRequestRescopedEvent.getPullRequest()).thenReturn(pullRequest);
        when(pullRequestUpdatedEvent.getPullRequest()).thenReturn(pullRequest);
    }

    @Test
    public void testOnPullRequestRescopedSourceCommitChanged() {
        String firstRef = "firstRef";
        String secondRef = "secondRef";

        // Setup sourceCommitChanged = TRUE
        when(pullRequestRescopedEvent.getPreviousFromHash()).thenReturn(firstRef);
        when(pullRequest.getFromRef().getLatestCommit()).thenReturn(secondRef);

        underTest.onPullRequestRescoped(pullRequestRescopedEvent);

        verify(pullRequestService).withdrawApproval(anyInt(), anyInt());
    }

    @Test
    public void testOnPullRequestRescopedSourceCommitUnchanged() {
        String firstRef = "ref";

        // Setup sourceCommitChanged = FALSE
        when(pullRequestRescopedEvent.getPreviousFromHash()).thenReturn(firstRef);
        when(pullRequest.getFromRef().getLatestCommit()).thenReturn(firstRef);

        underTest.onPullRequestRescoped(pullRequestRescopedEvent);

        verify(pullRequestService, never()).withdrawApproval(anyInt(), anyInt());
    }

    @Test
    public void testOnPullRequestUpdatedTargetBranchChanged() {
        when(pullRequestUpdatedEvent.getPreviousToBranch()).thenReturn(mock(Ref.class));

        underTest.onPullRequestUpdated(pullRequestUpdatedEvent);

        verify(pullRequestService).withdrawApproval(anyInt(), anyInt());
    }

    @Test
    public void testOnPullRequestUpdatedTargetBranchUnchanged() {
        when(pullRequestUpdatedEvent.getPreviousToBranch()).thenReturn(null);

        underTest.onPullRequestUpdated(pullRequestUpdatedEvent);

        verify(pullRequestService, never()).withdrawApproval(anyInt(), anyInt());
    }

    private PullRequestParticipant mockParticipant() {
        PullRequestParticipant pullRequestParticipant = mock(PullRequestParticipant.class);
        when(pullRequestParticipant.getUser()).thenReturn(mock(ApplicationUser.class));
        return pullRequestParticipant;
    }
}
